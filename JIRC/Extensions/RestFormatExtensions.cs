﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RestFormatExtensions.cs" company="David Bevin">
//   Copyright (c) 2013 David Bevin.
// </copyright>
// // <summary>
//   https://bitbucket.org/dpbevin/jira-rest-client-dot-net
//   Licensed under the BSD 2-Clause License.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Globalization;

namespace JIRC.Extensions
{
    public static class RestFormatExtensions
    {
        public static string ToRestString(this DateTimeOffset dt)
        {
            //Need datetime-format: 2013-10-28T12:26:55.000+0100
            string result=dt.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
            //Get format: 2013-10-28T12:26:55.000+01:00 => Remove colon (':') at the end
            result = result.Remove(result.Length - 3, 1);
            return result;
        }
    }
}
