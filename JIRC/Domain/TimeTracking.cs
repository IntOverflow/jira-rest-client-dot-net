﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JIRC.Domain
{
    public class TimeTracking
    {
        public string remainingEstimate { get; set; }
        public string timeSpent { get; set; }
        public string originalEstimate { get; set; }
        public int remainingEstimateSeconds { get; set; }
        public int timeSpentSeconds { get; set; }
        public int originalEstimateSeconds { get; set; }
    }
}
